package by.vanches.cat.dao;

import by.vanches.cat.model.Image;

/**
 * User: Vanja Novak
 * Date: 01.04.2016
 * Time: 23:44
 */

public interface ImageDao {
    Image getRandomImage();
}